import React from 'react';

//Dummy component that simply displays a table for info data
export default function InfoList(props: any){
    return(
        <table className='table'>
                    <thead>
                        <tr>
                            <th>Symbol</th>
                            <th>Gross Profit</th>
                            <th>Profit Margin</th>
                            <th>ROE</th>
                        </tr>
                    </thead>
                    <tbody>
                        {props.stocks_info.map((stock_info:any) =>{
                            return(
                                <tr key={stock_info.symbol}>
                                    <td>{stock_info.symbol} </td>
                                    <td>{stock_info.gross_profit} </td>
                                    <td>{stock_info.profit_margin} </td>
                                    <td>{stock_info.roe} </td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
    );
}