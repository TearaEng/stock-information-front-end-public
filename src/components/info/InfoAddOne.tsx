import React, {Component} from 'react';
import {Form , Button, Container, Row, Col} from 'react-bootstrap';
import {getJWT} from '../../helpers/jwt'
import InfoList from './InfoList'
import { toast } from 'react-toastify';

//Add Info data to the database
export class InfoAddOne extends Component<{}, {symbol: string, gross_profit: number, profit_margin: number, roe: number, resJSON: any}>{
        
    constructor(props:any){
        super(props);

        
        this.state = {
            symbol: '',
            gross_profit: 0.0,
            profit_margin: 0.0,
            roe: 0.0,
            resJSON: []
        }
    }

    change(event:any){
        if (event.target.name === "symbol"){
            this.setState({
                symbol: event.target.value
            })
        }
        else if(event.target.name === "gross_profit"){
            this.setState({
                gross_profit: event.target.value
            })
        }
        else if(event.target.name === "profit_margin"){
            this.setState({
                profit_margin: event.target.value
            })
        }
        else if(event.target.name === "roe"){
            this.setState({
                roe: event.target.value
            })
        }
    }

    submit(event:any){
        event.preventDefault();

        //Set up a custom header
        const JWT = getJWT();
        const myHeader = new Headers();
        myHeader.append('Content-Type', 'application/json');
        myHeader.append('Authorization', `Bearer ${JWT}`);

        fetch('http://18.218.237.163:5000/stocks/info',{
            method: 'POST',
            headers: myHeader,
            body: JSON.stringify({
                symbol: this.state.symbol,
                gross_profit: this.state.gross_profit,
                profit_margin: this.state.profit_margin,
                roe: this.state.roe
            })
        })
        .then(response => response.json())
        .then(data =>{
            toast.success(`Added ${this.state.symbol} to Stock Info`)
            this.setState({resJSON: data})
        })
        .catch((err) => toast.error('Uh Oh! Something Went Wrong!'))
    }

    render(){

        const myStyle={
            margin: "25px"
        }

        return(
            <>
                <Container fluid style={myStyle}>
                    <Row>
                        <Col lg={3}>
                            <form onSubmit={event => this.submit(event)}>
                                <Form.Group>
                                    <Form.Label>Symbol</Form.Label>
                                    <Form.Control
                                        type="text"
                                        name="symbol"
                                        placeholder="Symbol"
                                        onChange={event => this.change(event)}
                                        value={this.state.symbol}
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>Gross Profit</Form.Label>
                                    <Form.Control
                                        type="number"
                                        name="gross_profit"
                                        placeholder="Gross Profit"
                                        onChange={event => this.change(event)}
                                        value={this.state.gross_profit}
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>Profit Margin</Form.Label>
                                    <Form.Control
                                        type="number"
                                        name="profit_margin"
                                        placeholder="Profit Margin"
                                        onChange={event => this.change(event)}
                                        value={this.state.profit_margin}
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>ROE</Form.Label>
                                    <Form.Control
                                        type="number"
                                        name="roe"
                                        placeholder="ROE"
                                        onChange={event => this.change(event)}
                                        value={this.state.roe}
                                    />
                                </Form.Group>
                                <Button variant="primary" type="submit">
                                    Submit
                                </Button>
                            </form>
                        </Col>
                        <Col lg={9}>
                            <h3>Information That Has Been Added</h3>
                            <InfoList stocks_info={this.state.resJSON} />
                        </Col>
                    </Row>
                </Container>
            </>
        )
    }
}