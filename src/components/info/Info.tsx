import React, {Component} from 'react';
import InfoList from './InfoList'
import {getJWT} from '../../helpers/jwt'
import { toast } from 'react-toastify';

//Smart class that displays all the data from the info table
export class InfoFindAll extends Component <{}, { stocks_info: any }>{
    constructor(props:any){
        super(props);

        //what this component directly interacts with
        this.state = {
            stocks_info: []
        }
    }

    //Lifecycle method that starts when the page renders for the first time
    componentDidMount(){

        //Set up a custom header
        const JWT = getJWT();
        const myHeader = new Headers();
        myHeader.append('Content-Type', 'application/json');
        myHeader.append('Authorization', `Bearer ${JWT}`);

        //Make a get request to the API
        fetch('http://18.218.237.163:5000/stocks/info',{
            method: 'GET',
            headers: myHeader
        })
        .then(response => response.json())
        .then(data =>{
            toast.success('Now Showing All Stock Info')
            this.setState({stocks_info: data})
        })
        .catch((err) => toast.error('Uh Oh! Something Went Wrong!'))
    }

    //Render a table to show the information from the API
    render(){
        return(
            <>
                <InfoList stocks_info={this.state.stocks_info} />
            </>
        )
    }
}