import React from 'react';
import { NavLink } from 'react-router-dom';
import {Container, Row, Col, Image, Button} from 'react-bootstrap';
import Img from '../pictures/stocksIcon.png'

//Functional component that allows for a navbar at the top of the page
const Navbar = () => {

    function handleLogout(event:any){
        if(localStorage.getItem('JWT')){
            localStorage.removeItem('JWT')
        }
    }

    //If you're on the particular page it will have a different color
    const activeStyle = { color: 'orange' };
    
    //Setting a general style for displaying the information
    const mystyle = {
        margin: "0px 0px 0px 0px",
        padding: "15px 0px 15px 0px",
        backgroundColor: "Linen"
    }

    //Style for the logo itself
    const mystyle1 = {
        marginRight: "25px",
        marginLeft: "25px",
        marginBottom: "10px"
    }

    const mystyle2 = {
        display: "flex",
        justifyContent: "flex-end",
        marginRight: "15px",
      };

	return (
        <Container fluid style={mystyle}>
            <Row>
                <Col>
                    <nav>
                        <NavLink to='/home' activeStyle={activeStyle} exact><Image src={Img} style={mystyle1}/></NavLink>
                        {''}
                        <NavLink to='/infoFindAll' activeStyle={activeStyle} exact>InfoFindAll</NavLink>
                        {' | '}
                        <NavLink to='/infoFindOne' activeStyle={activeStyle}>InfoFindOne</NavLink>
                        {' | '}
                        <NavLink to='/infoAddOne' activeStyle={activeStyle}>InfoAddOne</NavLink>
                        {' | '}
                        <NavLink to='/infoDeleteOne' activeStyle={activeStyle}>InfoDeleteOne</NavLink>
                        {' | '}
                        <NavLink to='/s' activeStyle={activeStyle}>Stonks</NavLink>
                  </nav>
                </Col>
                <Col style={mystyle2}>
                    <Button variant='info' onClick={handleLogout}>Logout</Button>
                </Col>
            </Row>
        </Container>
	);
};

export default Navbar;