import React, {Component} from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import {Login} from './Login'
import Navbar from './NavBar'
import {ToastContainer} from 'react-toastify'

//Everything for the info endpoint
import {InfoFindAll} from './info/Info'
import {InfoFindOne} from './info/InfoFindOne'
import {InfoAddOne} from './info/InfoAddOne'
import {InfoDeleteOne} from './info/InfoDeleteOne'

//Everything for the socks endpoint
import {StocksFindAll} from './stocks/Stocks'
import {StocksFindOne} from './stocks/StocksFindOne'
import {StocksAddOne} from './stocks/StocksAddOne'
import {StocksDeleteOne} from './stocks/StocksDeleteOne'

//Misc components 
import PageNotFound from './PageNotFound'
import {StocksAllInOne} from './Experiement/StocksAllinOne'
import { Stonks } from './Stonks';

//Acts as controller with our routes
class App extends Component{
  render(){
    return(
      <BrowserRouter>
        <Navbar />
        <ToastContainer />
        <Switch>
          <Route path="/" exact component={Stonks} />
          <Route path="/home" exact component={Login} />

          <Route path="/infoFindAll" component={InfoFindAll} />
          <Route path="/infoFindOne" component={InfoFindOne} />
          <Route path="/infoAddOne" component={InfoAddOne} />
          <Route path="/infoDeleteOne" component={InfoDeleteOne} />

          <Route path="/stocksFindAll" component={StocksFindAll} />
          <Route path="/stocksFindOne" component={StocksFindOne} />
          <Route path="/stocksAddOne" component={StocksAddOne} />
          <Route path="/stocksDeleteOne" component={StocksDeleteOne} />

          <Route path="/s" component={StocksAllInOne} />
          <Route component={PageNotFound} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
