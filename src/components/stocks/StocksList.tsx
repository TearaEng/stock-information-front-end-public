import React from 'react';

//Dummy component that just displays a table for stocks
export default function StocksList(props: any){
    return(
        <table className='table'>
            <thead>
                <tr>
                    <th>Stock Name</th>
                    <th>Stock Symbol</th>
                    <th>High Price</th>
                    <th>Low Price</th>
                    <th>Adj Close</th>
                </tr>
            </thead>
            <tbody>
                {props.stocks.map((stock:any) =>{
                    return(
                        <tr key={stock.stock_symbol}>
                            <td>{stock.stock_name} </td>
                            <td>{stock.stock_symbol} </td>
                            <td>{stock.high_price} </td>
                            <td>{stock.low_price} </td>
                            <td>{stock.adjClose} </td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    );
}