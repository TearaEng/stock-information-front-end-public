import React, {Component} from 'react';
import {Form , Button, Container, Row, Col} from 'react-bootstrap';
import {getJWT} from '../../helpers/jwt'
import StocksList from './StocksList'
import { toast } from 'react-toastify';

//Smart Component that allows the user to specify what information they want to see
export class StocksFindOne extends Component<{}, {symbol: string, info: any}>{
        
    constructor(props:any){
        super(props);

        this.state = {
            symbol: '',
            info: []
        }
    }

   //change handler for when the user types things into fields
   change(event:any){
        this.setState({symbol: event.target.value})
    }

    //Submit handler to take the input and send it as a json file. Then get back a response with the JWT
    submit(event:any){
        event.preventDefault();

        //Set up a custom header
        const JWT = getJWT();
        const myHeader = new Headers();
        myHeader.append('Content-Type', 'application/json');
        myHeader.append('Authorization', `Bearer ${JWT}`);

        const symbol = this.state.symbol;
        let url = ('http://18.218.237.163:5000/stocks/')
        url = url.concat(symbol);

        //Fetch API call at the specified URL
        fetch(url, {
            method: 'GET',
            headers: myHeader
        })
        //Convert the response into a Json
        .then(response => response.json())
        //Then take the data and set it to a state
        .then((data) => {
            toast.success(`Now Showing ${symbol} Stocks`)
            this.setState({info: data})
        })
        //Catch an error if there is one
        .catch((err) => toast.error('Uh Oh! Something Went Wrong!'))
    }

    //render forms to get user input
    render(){

        const myStyle={
            margin: "25px"
        }

        return(
            <div>
                <Container fluid style={myStyle}>
                    <Row>
                        <Col lg={3}>
                            <form onSubmit={event => this.submit(event)}>
                                <Form.Group controlId = "formBasicInput">
                                    <Form.Label>Symbol</Form.Label>
                                    <Form.Control 
                                        type="text" 
                                        name="symbol"
                                        placeholder="Symbol" 
                                        onChange={event => this.change(event)}
                                        value={this.state.symbol}
                                    />
                                </Form.Group>
                                <Button variant="primary" type="submit">Submit</Button>
                            </form>
                        </Col>
                        <Col lg={9}>
                            <h3>Stock Info</h3>
                            <StocksList stocks={this.state.info} />
                        </Col>
                    </Row>
                </Container>
            </div>
       )
    }
}