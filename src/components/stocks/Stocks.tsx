import React, {Component} from 'react';
import {Container, Row, Col} from 'react-bootstrap';
import StocksList from './StocksList'
import {getJWT} from '../../helpers/jwt'
import { toast } from 'react-toastify';

//Smart Component that returns all of the data on stocks
export class StocksFindAll extends Component <{}, { stocks: any }>{
    constructor(props:any){
        super(props);

        //what this component directly interacts with
        this.state = {
            stocks: []
        }
    }

    //Lifecycle method that starts when the page renders for the first time
    componentDidMount(){

        //Set up a custom header
        const JWT = getJWT();
        const myHeader = new Headers();
        myHeader.append('Content-Type', 'application/json');
        myHeader.append('Authorization', `Bearer ${JWT}`);

        //Make a get request to the API
        fetch('http://18.218.237.163:5000/stocks',{
            method: 'GET',
            headers: myHeader
        })
        .then(response => response.json())
        .then(data =>{
            toast.success('Now Showing All Stocks')
            this.setState({stocks: data})
        })
        .catch((err) => toast.error('Uh Oh! Something Went Wrong!'))
    }

    //Render a table to show the information from the API
    render(){
        return(
            <>
                <Container fluid>
                    <Row>
                        <Col>
                            <StocksList stocks={this.state.stocks} />
                        </Col>
                    </Row>
                </Container>
            </>
        )
    }
}