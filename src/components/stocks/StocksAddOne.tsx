import React, {Component} from 'react';
import {Form , Button, Container, Row, Col} from 'react-bootstrap';
import {getJWT} from '../../helpers/jwt'
import StocksList from './StocksList'
import { toast } from 'react-toastify';

//Smart Component that allows the user to add data to the stocks table
export class StocksAddOne extends Component<{}, {stock_name: string, stock_symbol: string, high_price: number, low_price: number, adjClose: number, resJSON: any}>{
        
    constructor(props:any){
        super(props);

        
        this.state = {
            stock_name:'',
            stock_symbol: '',
            high_price: 0.0,
            low_price: 0.0,
            adjClose: 0.0,
            resJSON: []
        }
    }

    //handles the reflection of change for when the user types
    change(event:any){
        if (event.target.name === "stock_name"){
            this.setState({
                stock_name: event.target.value
            })
        }
        else if (event.target.name === "stock_symbol"){
            this.setState({
                stock_symbol: event.target.value
            })
        }
        else if(event.target.name === "high_price"){
            this.setState({
                high_price: event.target.value
            })
        }
        else if(event.target.name === "low_price"){
            this.setState({
                low_price: event.target.value
            })
        }
        else if(event.target.name === "adjClose"){
            this.setState({
                adjClose: event.target.value
            })
        }
    }

    //Handles an on submit that sends a fetch request to the api and returns data from the database
    submit(event:any){
        event.preventDefault();

        //Set up a custom header
        const JWT = getJWT();
        const myHeader = new Headers();
        myHeader.append('Content-Type', 'application/json');
        myHeader.append('Authorization', `Bearer ${JWT}`);

        fetch('http://18.218.237.163:5000/stocks',{
            method: 'POST',
            headers: myHeader,
            body: JSON.stringify({
                stock_name: this.state.stock_name,
                stock_symbol: this.state.stock_symbol,
                high_price: this.state.high_price,
                low_price: this.state.low_price,
                adjClose: this.state.adjClose
            })
        })
        .then(response => response.json())
        .then(data =>{
            this.setState({resJSON: data})
            toast.success(`Added ${this.state.stock_name} to Stocks`)
        })
        .catch((err) => toast.error('Uh Oh! Something Went Wrong!'))
    }

    render(){
        const myStyle={
            margin: "25px"
        }

        return(
            <>
                <Container fluid style={myStyle}>
                    <Row>
                        <Col lg={3}>
                            <form onSubmit={event => this.submit(event)}>
                                <Form.Group>
                                    <Form.Label>Stock Name</Form.Label>
                                    <Form.Control
                                        type="text"
                                        name="stock_name"
                                        placeholder="Stock Name"
                                        onChange={event => this.change(event)}
                                        value={this.state.stock_name}
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>Stock Symbol</Form.Label>
                                    <Form.Control
                                        type="text"
                                        name="stock_symbol"
                                        placeholder="Stock Symbol"
                                        onChange={event => this.change(event)}
                                        value={this.state.stock_symbol}
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>High Price</Form.Label>
                                    <Form.Control
                                        type="number"
                                        name="high_price"
                                        placeholder="High Price"
                                        onChange={event => this.change(event)}
                                        value={this.state.high_price}
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>Low Price</Form.Label>
                                    <Form.Control
                                        type="number"
                                        name="low_price"
                                        placeholder="Low Price"
                                        onChange={event => this.change(event)}
                                        value={this.state.low_price}
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>ADJ Close</Form.Label>
                                    <Form.Control
                                        type="number"
                                        name="adjClose"
                                        placeholder="ADJ Close"
                                        onChange={event => this.change(event)}
                                        value={this.state.adjClose}
                                    />
                                </Form.Group>
                                <Button variant="primary" type="submit">
                                    Submit
                                </Button>
                            </form>
                        </Col>
                        <Col lg={9}>
                            <h3>Stock That Has Been Added</h3>
                            <StocksList stocks={this.state.resJSON} />
                        </Col>
                    </Row>
                </Container>
            </>
        )
    }
}