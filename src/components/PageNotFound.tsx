import React from 'react';
import { Link } from 'react-router-dom';
import {Button, Container, Row, Col} from 'react-bootstrap';

//Functional component to show a webpage that does not exist out of the provided urls
export default function NotFoundPage() {

    const mystyle = {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "975px",
      };

	return (
		<>
            <Container className="flex align-center" style={mystyle}>
                <Row>
                    <Col>
                        <h1>404. Page not found</h1>
                        <Link to='/'>
                            <Button className='btn btn-warning'>Back to home</Button>
                        </Link>
                    </Col>
                </Row>
            </Container>
		</>
	);
}
