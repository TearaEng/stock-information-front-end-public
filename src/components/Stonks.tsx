import React, {Component} from 'react';
import {Modal, Form , Button, Container, Row, Col, Image} from 'react-bootstrap';

import Img from '../pictures/stocks.png'
import Img2 from '../pictures/Stonks_Meme.jpg'
import { toast } from 'react-toastify';

//Component for Login functionality 
export class Stonks extends Component <{}, { username: string, password: string, modalShow: boolean}> {

    //Constructor to create a state for an empty username and password
    constructor(props:any){
        super(props);

        //what this component directly interacts with
        this.state = {
            username: '',
            password: '',
            modalShow: false,
        }

        this.change = this.change.bind(this);
    }
    handleClose = () => this.setState({modalShow: false});
    handleShow = () => this.setState({modalShow: true});

    //change handler for when the user types things into fields
    change(event:any){
        if (event.target.name === "username"){
            this.setState({
                username: event.target.value
            })
        }
        else{
            this.setState({
                password: event.target.value
            })
        }

    }

    //Submit handler to take the input and send it as a json file. Then get back a response with the JWT
    submit(event:any){
        event.preventDefault();

        //Fetch API call at the specified URL
        fetch('http://18.218.237.163:5000/login',{
            //Specify we're making a POST request
            method: 'POST',
            //Header specifies that we're sending a JSON
            headers: {
                'Content-Type': 'application/json',
            },
            //Pass a JSON through the body with our information
            body: JSON.stringify({
                username: this.state.username,
                password: this.state.password
            })
        })
        //Convert the response into a Json
        .then(response => response.json())
        //Then take the data which is the JWT and store in local storage
        .then((data) => {
            toast.success('Logged In!')
            localStorage.setItem('JWT', data.accessToken)
        })
        //Catch an error if there is one
        .catch((err) => toast.error('Hmm Not Quite...'))
    }

    //render forms to get user input
    render(){

          const mystyle = {
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "250px",
          };

          const mystyle2 = {
              marginRight: "100px",
              marginLeft: "130px"
          }

          const mystyle3 = {
            marginLeft: "55px"
        }

          const mystyle4 = {
            height: "250px",
          };

          const check = {
                backgroundRepeat: 'no-repeat',
                backgroundImage: `url(${Img2})`,
                backgroundColor: "DarkBlue",
                backgroundBlendMode: 'overlay',
                height: "890px",
          }

          const text = {
            color: 'White'
          }
        return(
            <div style={check} className= "app flex-row align-items-center">
                <Modal show={this.state.modalShow} onHide={(event: any) => this.handleClose()}>
                    <Modal.Header closeButton>
                        <Modal.Title>Login</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <form onSubmit={event => this.submit(event)}>
                            <Form.Group controlId="formBasicUsername">
                                <Form.Label>Username</Form.Label>
                                <Form.Control 
                                    type="text" 
                                    name = "username"
                                    placeholder = "Username" 
                                    onChange={event => this.change(event)}
                                    value={this.state.username}
                                    required 
                                />
                            </Form.Group>

                            <Form.Group controlId="formBasicPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control 
                                    type="password" 
                                    name = "password"
                                    placeholder = "Password" 
                                    onChange={event => this.change(event)}
                                    value={this.state.password}
                                    required 
                                />
                            </Form.Group>
                            <Button variant="primary" type="submit" onClick={(event:any) => this.handleClose()}>
                                Submit
                            </Button>
                        </form>
                    </Modal.Body>
                </Modal>
                <Container>
                    <Row style={mystyle4}></Row>
                    <Row style={mystyle}>
                        <Col style={mystyle2} lg={3}>
                            <h1 style={text}>Welcome to Stonks Information</h1> 
                        </Col>
                        <Col lg={2}>
                            <Image src={Img}/>
                        </Col>
                        <Col style={mystyle3} lg={4}>
                            <Button variant="primary" type="submit" onClick={(event:any) => this.handleShow()}>
                                        Get Started
                            </Button>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}