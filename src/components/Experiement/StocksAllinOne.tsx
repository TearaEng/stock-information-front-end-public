import React, {Component} from 'react';
import {Form , Button, Container, Row, Col, Dropdown} from 'react-bootstrap';
import StocksList from '../stocks/StocksList'
import {getJWT} from '../../helpers/jwt'
import {toast} from 'react-toastify'

//Don't looks at this. I was just trying to figure dropdowns
export class StocksAllInOne extends Component <{}, { stocks: any, showOne: boolean, symbol: string, showAdd: boolean, showDelete: boolean, stock_name: string, high_price: number, low_price: number, adjClose: number}>{
    constructor(props:any){
        super(props);

        //what this component directly interacts with
        this.state = {
            stocks: [],
            symbol: "",
            stock_name:'',
            high_price: 0.0,
            low_price: 0.0,
            adjClose: 0.0,
            showOne: false,
            showAdd: false,
            showDelete: false,
        }
    }

    change(event:any){
        if (event.target.name === "stock_name"){
            this.setState({
                stock_name: event.target.value
            })
        }
        else if (event.target.name === "symbol"){
            this.setState({
                symbol: event.target.value
            })
        }
        else if(event.target.name === "high_price"){
            this.setState({
                high_price: event.target.value
            })
        }
        else if(event.target.name === "low_price"){
            this.setState({
                low_price: event.target.value
            })
        }
        else if(event.target.name === "adjClose"){
            this.setState({
                adjClose: event.target.value
            })
        }
    }

///////////////////////////////////////////////////////////////////////////////////////////

    handleShowOne(event:any){
        this.setState({showOne: true});
    }

    handleHideOne(event:any){
        this.setState({showOne: false});
    }

    handleShowAdd(event:any){
        this.setState({showAdd: true});
    }

    handleHideAdd(event:any){
        this.setState({showAdd: false});
    }

    handleShowDelete(event:any){
        this.setState({showDelete: true});
    }

    handleHideDelete(event:any){
        this.setState({showDelete: false});
    }

/////////////////////////////////////////////////////////////////////////////////////////////

    handleShowOneForm(){
        return(
            <form onSubmit={event => this.handleFindOne(event)}>
            <Form.Group controlId = "formBasicInput">
                <Form.Label>Symbol</Form.Label>
                <Form.Control 
                    type="text" 
                    name="symbol"
                    placeholder="Symbol" 
                    onChange={event => this.change(event)}
                    value={this.state.symbol}
                />
            </Form.Group>
            <Button variant="primary" type="submit">Submit</Button>
        </form>
        )
    }

    handleAddForm(){
        return(
            <form onSubmit={event => this.handleAddOne(event)}>
            <Form.Group>
                <Form.Label>Stock Name</Form.Label>
                <Form.Control
                    type="text"
                    name="stock_name"
                    placeholder="Stock Name"
                    onChange={event => this.change(event)}
                    value={this.state.stock_name}
                />
            </Form.Group>
            <Form.Group>
                <Form.Label>Stock Symbol</Form.Label>
                <Form.Control
                    type="text"
                    name="symbol"
                    placeholder="Stock Symbol"
                    onChange={event => this.change(event)}
                    value={this.state.symbol}
                />
            </Form.Group>
            <Form.Group>
                <Form.Label>High Price</Form.Label>
                <Form.Control
                    type="number"
                    name="high_price"
                    placeholder="High Price"
                    onChange={event => this.change(event)}
                    value={this.state.high_price}
                />
            </Form.Group>
            <Form.Group>
                <Form.Label>Low Price</Form.Label>
                <Form.Control
                    type="number"
                    name="low_price"
                    placeholder="Low Price"
                    onChange={event => this.change(event)}
                    value={this.state.low_price}
                />
            </Form.Group>
            <Form.Group>
                <Form.Label>ADJ Close</Form.Label>
                <Form.Control
                    type="number"
                    name="adjClose"
                    placeholder="ADJ Close"
                    onChange={event => this.change(event)}
                    value={this.state.adjClose}
                />
            </Form.Group>
            <Button variant="primary" type="submit">
                Submit
            </Button>
        </form>
        )
    }

    handleDeleteOneForm(){
        return(
            <form onSubmit={event => this.handleDeleteOne(event)}>
            <Form.Group controlId = "formBasicInput">
                <Form.Label>Symbol</Form.Label>
                <Form.Control 
                    type="text" 
                    name="symbol"
                    placeholder="Symbol" 
                    onChange={event => this.change(event)}
                    value={this.state.symbol}
                />
            </Form.Group>
            <Button variant="primary" type="submit">Submit</Button>
        </form>
        )
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    //Lifecycle method that starts when the page renders for the first time
    handleSelectAll(event:any){   
        event.preventDefault();

        //Set up a custom header
        const JWT = getJWT();
        const myHeader = new Headers();
        myHeader.append('Content-Type', 'application/json');
        myHeader.append('Authorization', `Bearer ${JWT}`);

        //Make a get request to the API
        fetch('http://18.218.237.163:5000/stocks',{
            method: 'GET',
            headers: myHeader
        })
        .then(response => response.json())
        .then(data =>{
            toast.success('Now Showing All Stocks')
            this.setState({stocks: data})
        })
        .catch((err) => toast.error('Uh Oh! Something Went Wrong!'))
    }

    handleFindOne(event:any){
        event.preventDefault();

        //Set up a custom header
        const JWT = getJWT();
        const myHeader = new Headers();
        myHeader.append('Content-Type', 'application/json');
        myHeader.append('Authorization', `Bearer ${JWT}`);

        const symbol = this.state.symbol;
        let url = ('http://18.218.237.163:5000/stocks/')
        url = url.concat(symbol);

        //Fetch API call at the specified URL
        fetch(url, {
            method: 'GET',
            headers: myHeader
        })
        //Convert the response into a Json
        .then(response => response.json())
        //Then take the data and set it to a state
        .then((data) => {
            toast.success(`Now Showing ${this.state.symbol} Stocks`)
            this.setState({stocks: data})
        })
        //Catch an error if there is one
        .catch((err) => toast.error('Uh Oh! Something Went Wrong!'))
    }

    handleAddOne(event:any){
        event.preventDefault();

        //Set up a custom header
        const JWT = getJWT();
        const myHeader = new Headers();
        myHeader.append('Content-Type', 'application/json');
        myHeader.append('Authorization', `Bearer ${JWT}`);

        fetch('http://18.218.237.163:5000/stocks',{
            method: 'POST',
            headers: myHeader,
            body: JSON.stringify({
                stock_name: this.state.stock_name,
                stock_symbol: this.state.symbol,
                high_price: this.state.high_price,
                low_price: this.state.low_price,
                adjClose: this.state.adjClose
            })
        })
        .then(response => response.json())
        .then(data =>{
            this.setState({stocks: data})
            toast.success(`Added ${this.state.stock_name} to Stocks`)
        })
        .catch((err) => toast.error('Oh Noses! Something Went Wrong!'))
    }

    handleDeleteOne(event:any){
        event.preventDefault();

        //Set up a custom header
        const JWT = getJWT();
        const myHeader = new Headers();
        myHeader.append('Content-Type', 'application/json');
        myHeader.append('Authorization', `Bearer ${JWT}`);

        const symbol = this.state.symbol;
        let url = ('http://18.218.237.163:5000/stocks/')
        url = url.concat(symbol);

        //Fetch API call at the specified URL
        fetch(url, {
            method: 'DELETE',
            headers: myHeader
        })
        //Convert the response into a Json
        .then(response => response.json())
        //Then take the data and set it to a state
        .then((data) => {
            toast.success(`Deleted ${this.state.symbol} from Stocks`)
            this.setState({stocks: data})
        })
        //Catch an error if there is one
        .catch((err) => toast.error('Uh Oh! Something Went Wrong!'))
    }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //Render a table to show the information from the API
    render(){

        const background={
            height: "885px",
            backgroundColor: "DarkBlue"
        }
        const container={
            height: "885px",
            backgroundColor: "White",
        }

        const myStyle={
            padding: "15px"
        }

        const myStyle1={
            marginTop: "15px",
        }

        const myStyle2={
            marginBottom: "15px",
        }
        
        return(
            <div style={background}>
                <Container style={container}>
                    <Row>
                        <Col lg={4} style={myStyle}>
                            <Dropdown>
                                <Dropdown.Toggle variant="primary" id="basicDropdown" style={myStyle2}>Actions</Dropdown.Toggle>
                                <Dropdown.Menu>
                                    <Dropdown.Item onClick={(event:any) => {
                                        this.handleHideOne(event)
                                        this.handleHideAdd(event)
                                        this.handleHideDelete(event)
                                        this.handleSelectAll(event)
                                        }}>Show All</Dropdown.Item>

                                    <Dropdown.Item onClick={(event:any) => {
                                        this.handleHideAdd(event)
                                        this.handleHideDelete(event)
                                        this.handleShowOne(event)
                                        }}>Find One</Dropdown.Item>

                                    <Dropdown.Item onClick={(event:any) => {
                                        this.handleHideOne(event)
                                        this.handleHideDelete(event)
                                        this.handleShowAdd(event)
                                        }}>Add One</Dropdown.Item>

                                    <Dropdown.Item onClick={(event:any) => {
                                        this.handleHideAdd(event)
                                        this.handleHideOne(event)
                                        this.handleShowDelete(event)
                                        }}>Delete One</Dropdown.Item>
                                </Dropdown.Menu>
                            </Dropdown>
                            {this.state.showOne ? this.handleShowOneForm() : null}
                            {this.state.showAdd ? this.handleAddForm() : null}
                            {this.state.showDelete ? this.handleDeleteOneForm() : null}
                        </Col>

                        <Col style={myStyle1} lg={8}>
                            <StocksList stocks={this.state.stocks} />
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}