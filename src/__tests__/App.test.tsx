import React from 'react';
import App from '../components/App';
import {shallow} from 'enzyme';
import { Route } from 'react-router';
import { InfoFindAll } from '../components/info/Info';

describe('App', function() {
  it('Should render without throwing an error', function() {
    expect(shallow(<App />).contains(<Route path="/infoFindAll" component={InfoFindAll} />)).toBe(true);
  });

});