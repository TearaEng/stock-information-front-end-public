import React from 'react'
import {shallow} from 'enzyme'
import StocksList from '../components/stocks/StocksList'

describe("StocksList Component", ()=>{

    const tables = [
        {
            "stock_name": "Facebook., Inc",
            "stock_symbol": "FB",
            "high_price": "210.78",
            "low_price": "202.11",
            "adjClose": "204.27"
        },
        {
            "stock_name": "Apple Inc.",
            "stock_symbol": "AAPL",
            "high_price": "315.95",
            "low_price": "303.21",
            "adjClose": "305.47"
        },
        {
            "stock_name": "amazonserver",
            "stock_symbol": "AWS",
            "high_price": "220.78",
            "low_price": "211.11",
            "adjClose": "99.27"
        },
        {
            "stock_name": "Java/React",
            "stock_symbol": "symbol",
            "high_price": "5.00",
            "low_price": "4.00",
            "adjClose": "4.50"
        }
    ]

    const wrapper = shallow(<StocksList stocks={tables}/>);

    it('Should Show Correct Tables', ()=>{
            const rows = wrapper.find('tr');
            expect(rows.length).toBe(5);
    })

})