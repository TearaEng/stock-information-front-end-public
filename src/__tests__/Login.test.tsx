import React from 'react'
import Enzyme, {shallow} from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import {Login} from '../components/Login'

Enzyme.configure({adapter:new Adapter()})

describe("Login Component", ()=>{
    it('Should Render', ()=>{
            //Allows the page to show without having to mount onto the DOM
            const wrapper = shallow(<Login/>);
            const div = wrapper.find('div');
            expect(div.length).toBe(1);
    })
    it('Should Have a Button', ()=>{
        const wrapper = shallow(<Login/>)
        const buttons = wrapper.find('Button')
        expect(buttons.length).toBe(2);
    })
})