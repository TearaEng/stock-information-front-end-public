import React from 'react'
import { Button } from 'react-bootstrap';
import NotFoundPage from '../components/PageNotFound';
import { shallow } from 'enzyme';

describe('404', function() {
    it('Should render without throwing an error', function() {
      expect(shallow(<NotFoundPage />).contains(<Button className='btn btn-warning'>Back to home</Button>)).toBe(true);
    });
  
  });