# Stocks Information Front End

## Web Folder structure

- public
    - Base files that come with doing create-react-app
- src
    - tests
        - App.test.tsx: Tests App.tsx
        - Stocks.test.tsx: Tests Stocks.tsx
    - components: Contains the bulk of the project that use consume the API at certain points
        - Experiement
            - StocksAllinOne.tsx: A test of using drop down menu to select actions
        - info: Contains all the components that use the Info Route
            - Info.tsx
            - InfoAddOne.tsx
            - InfoDeleteOne.tsx
            - InfoFindOne.tsx
            - InfoList.tsx: Just a dummy component that returns a table
        - stocks: Contains all the components that use the Stocks Route
            - Stocks.tsx
            - StocksAddOne.tsx
            - StocksFindOne.tsx
            - StocksList.tsx: Just a dummy component that returns a table
        - helpers
            - jwt.tsx: returns the JWT in the localstorage
        - pictures
            - background.png: used for the background of the landing page
            - stocks.png: used on the landing page
            - stocksIcon.png: used for the navbar to act as a home button
    - App.css
    - index.css
    - index.tsx
    - logo.svg
    - react-app-env.d.ts
    - serviceWorker.ts
    - setupTests.ts
    - package-lock.json
    - package.json
    - tsconfig.json
 
## User Input
### Input
When on the landing page, users can click on the "Get Started" button to open up a modal for login. This is where they enter their username and password and get a confirmation of whether or not they're logged in

![alt text][login]

[login]: ./src/pictures/login.PNG "Login Picture"

### FindAll
When the action is chosen then it will automatically show all the data on the table from the stocks table from the database

![alt text][All]

[All]: ./src/pictures/showAll.PNG "Find All"

### FindOne
When chosen you can input a symbol that will then only show data for one row of the stocks table

![alt text][One]

[One]: ./src/pictures/FindOne.PNG "Find One"

### AddOne
When chosen you can input a stock name, symbol, high price, low price, and agj close. The table will show your input

![alt text][Add]

[Add]: ./src/pictures/AddOne.PNG "Add One"

### DeleteOne
When chosen you can input a symbol it will delete that particular row from the database

![alt text][Delete]

[Delete]: ./src/pictures/DeleteOne.PNG "Delete One"

## Project 1 Minimum Requirements
- [x] A front end client that utilizes at least 4 of the routes from your teammates web service (not including authentication endpoints)
- [x] Front end should be able to successfully query and perform transactions to the DB via the web service
- [x] Make minor revisions to the API that you wrote (if applicable)
- [x] Deploy Express server on AWS EC2
- [x] Create a pipeline using Jenkins that achieves continuous delivery
- [x] Documentation for front end (you can also fix your documentation for your backend if needed)
- [ ] Unit Testing for React Components (3 components tested)
- [x] Login Functionality
- [x] Follow basic Scrum/Agile practices: Sprint Planning Meeting, User Stories, Scrum Board, Standup meetings